#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'json'
require 'base64'

API_URL = 'https://hackerone.com/graphql'
HEADERS = {
  "content-type": 'application/json',
  "X-Auth-Token": ARGV[0]
}.freeze

def query(body)
  response = HTTParty.post(API_URL, body: JSON.generate(body), headers: HEADERS)

  abort(response['errors'].join) unless response['errors'].nil?

  response
end

def subscribe(team_handle)
  query(
    {
      operationName: 'UpdateSubscription',
      variables: { handle: team_handle },
      query: "
        mutation UpdateSubscription($handle: String!) {
          toggleTeamUpdatesSubscription(input: {handle: $handle}) {
            was_successful
            team {
              id
              subscribed
              __typename
            }
            __typename
          }
        }"
    }
  )

  puts "[+] Subscribed to #{team_handle}"
end

cursor = ''
loop do
  puts "[*] Querying for 50 programs starting at #{cursor.empty? ? '0' : Base64.decode64(cursor)}"
  response = query(
    {
      operationName: 'GetTeams',
      variables: '',
      query: "
        query GetTeams {
          teams(after: \"#{cursor}\", first: 50) {
            pageInfo {
              hasNextPage
            }
            edges {
              cursor
              node {
                handle
                state
                offers_bounties
                subscribed
              }
            }
          }
        }"
    }
  )

  response['data']['teams']['edges'].each do |edge|
    cursor = edge['cursor']
    node = edge['node']
    next if node['subscribed']
    # Comment out the next line if you want to also include public programs
    next unless node['state'] == 'soft_launched'
    # Comment out the next line if you want to include VDPs
    next unless node['offers_bounties']

    subscribe(node['handle'])
  end

  break unless response['data']['teams']['pageInfo']['hasNextPage']
end

puts '[+] Done!'
